(function () {
  'use strict';

  angular
    .module('staySelect.routes')
    .config(indexConfig);

  indexConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
  
/**
   * @name config
   * @desc Define valid application routes*/

   //***  stateProvider for Symptoms*****
  window.static_path = 'http://127.0.0.1:8000/static/';
  /*window.static_path = 'https://healthbrio.s3.amazonaws.com/';*/

  function indexConfig($stateProvider, $urlRouterProvider){

    $urlRouterProvider.otherwise('/home');
    $stateProvider
        .state('home', {
            url: '/home',
            controller:'PrimaryIndexController',
            controllerAs:'primary_vm',
            templateUrl: static_path+'templates/index/primary_index.html'
        })
        .state('hotels',{
            url:'/hotels',
            controller:'HotelsController',
            controllerAs:'hotelsCtrl_vm',
            //template:'<h1>Hotel Page is herw</h1>'
            templateUrl: static_path +'templates/hotels/hotels.html'
        })
        .state('hotel',{
            url:'/hotel/id',
            controller:'HotelController',
            controllerAs:'hotalCtrl_vm',
            //template:'<h1>Hotel Description here</h1>',
            templateUrl:static_path + 'templates/hotel/hotel.html'
        })
  }


})();