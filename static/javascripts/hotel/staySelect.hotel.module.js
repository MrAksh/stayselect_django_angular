(function(){
    'use strict';

    angular.module('staySelect.hotel',[
       'staySelect.hotel.controllers',
        'staySelect.hotel.directives',
        'staySelect.hotel.services'
    ]);

    angular.module('staySelect.hotel.controllers',[]);
    angular.module('staySelect.hotel.directives',[]);
    angular.module('staySelect.hotel.services',[]);
})();