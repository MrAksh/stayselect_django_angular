(function(){
    'use strict';

    angular.module('staySelect.hotels',[
       'staySelect.hotels.controllers',
        'staySelect.hotels.directives',
        'staySelect.hotels.services'
    ]);

    angular.module('staySelect.hotels.controllers',[]);
    angular.module('staySelect.hotels.directives',[]);
    angular.module('staySelect.hotels.services',[]);
})();