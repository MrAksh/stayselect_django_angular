

/*angular.module('myApp', ['ngRoute'])
    .config(['$routeProvider', function($routeProvider){
        $routeProvider.when('/symptoms', {templateUrl: 'templates/symptoms/symptoms.html'})
        .when('/conditions', {templateUrl: 'templates/conditions/conditions.html'})

    }]);*/



(function () {
  'use strict';

  var app=angular
    .module('staySelect', [
      //'staySelect.config',

      //'ngAnimate',
	  'staySelect.routes',
      'staySelect.primary',
      'staySelect.hotels',
      'staySelect.hotel'
      
    ]);

  angular.module('staySelect.config', []);

  angular.module('staySelect.routes', ['ngRoute', 'ui.router']).run(function($rootScope) {
    /*$rootScope.static_path = 'https://healthbrio.s3.amazonaws.com/';*/
    $rootScope.static_path = 'http://127.0.0.1:8000/static/';
  });

})();






